var Router = require('./router.js'),
    colors = require('colors')

Router = new Router({},{mode:'object'})

Router.addRoutes({
  'test.route': function(){console.log('foo')}
})

Router.addRoutes({
  'test.route': function(){console.log('bar')},
  'test[:baz].lol': [
    function(baz){console.log(baz)},
    function(baz){console.log(baz.red, baz, baz.blue)}
  ],
  "test[:h\"'''\"at]": [
    function(baz){console.log(baz)},
    function(baz){console.log(baz.green.bold, baz.yellow, baz)}
  ],
})

console.log(Router.getRoutes())

Router.route('test.route')
