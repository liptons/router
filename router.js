/*
  respects two types of paths
    a) url/style/paths
    b) object.style.paths
      b.1) object[style].paths
    c?) {curly} braces...
        I'm not sure if I wan to continuer down this path.
        The cury braces are MUCH more flexible but I feel
        like thy emight be too flexable. There are so many
        special cases that curly braces become unpredictable.

  variable path nodes are deliniated with a ':' character

*/


/*
  Closures
*/
var modes = {
    curly: {
      variableDeliniator: '{',
      joinPartsWith: "+",
      splitter: /((?!^)\{.*?\})/,
      variableString: "(.*)",
      normalize: function(route){
        return route
      }
    },
    object: {
      variableDeliniator: ':',
      joinPartsWith: '+[\\.]',
      splitter: /\./,
      variableString: "(.*)",
      normalize: function(route){
        return route.split(/((?!^)\[.*?\])/).filter(Boolean).map(function(s){
          return s.replace(/[\[\]\'\"]/g,'')
        }).join('.')
      }
    },
    url: {
      variableDeliniator: ':',
      joinPartsWith: '+[\\/]',
      splitter: '/',
      variableString: "(.*)",
      normalize: function(route){

        if(route[route.length - 1] === '/')
          return route.substring(0, route.length - 1).replace(/[\'\"]/, '')

        return route
      }
    }
}

var Router = function(initialRoutes, options){

  /*
    Private
  */
  var options = Object.assign({mode: 'url'}, options),
      normalize = modes[options.mode].normalize,
      variableDeliniator = modes[options.mode].variableDeliniator,
      splitter = modes[options.mode].splitter,
      joinPartsWith = modes[options.mode].joinPartsWith,
      routes = {},
      regexTests = {},
      variableString = modes[options.mode].variableString,
      getArgs = function(route, string){
        var nodes = route.split(splitter),
            splitInput = regexTests[route].exec(string),
            args = []


        for(var i = 0; i < nodes.length; i++){
          // I hate this. This is a symptom of a bigger problem. My regex shouldn't be returning the curly braces.
          if(options.mode === 'curly') {
            splitInput[i + 1] = splitInput[i + 1].replace(/[{}]/g, "")
          }

          if(nodes[i][0] === variableDeliniator) args.push(splitInput[i + 1])
        }

        return args
      },
      regexFromPart = function(part) {
        if(part[0] === variableDeliniator) return variableString
        return "("+part+")"
      },
      generateRegexTests = function(){
        for(var route in routes){
          route = normalize(route)
          var parts = route.split(splitter),
              test = new RegExp(parts.map(regexFromPart).join(joinPartsWith))
          regexTests[route] = test
        }
      },
      getMatches = function(string){
        var matches = []
        string = normalize(string)
        for(var route in routes){
          route = normalize(route)
          var match = string.match(regexTests[route]),
              nodes = string.split(splitter),
              routeNodes = route.split(splitter);

          if(!match || (string.split(splitter).length !== route.split(splitter).length)) continue
          matches.push({methods: routes[route],arguments: getArgs(route, string)})

        }
        return matches
      }

  /*
    getters / setters
  */
  this.addRoute = function(route, handler){

    route = normalize(route)

    if(routes[route] === undefined) routes[route] = []

    if(typeof routes[route] === 'function') routes[route] = [routes[route]]

    if(Array.isArray(routes[route]) && Array.isArray(handler)) {
      return handler.map(function(h){
        this.addRoute(route, h)
      }.bind(this))
      return
    }

    if(Array.isArray(routes[route])) routes[route].push(handler)

  }.bind(this)

  this.addRoutes = function(newRoutes){
    for(var route in newRoutes) this.addRoute(route, newRoutes[route])
    generateRegexTests()
  }
  this.getRoutes = function(){return routes}
  this.getTests = function(){return regexTests}
  this.route = function(route){
    route = normalize(route)
    var matches = getMatches(route)
    for(var match in matches) matches[match].methods.map(function(handler){handler.apply(this, matches[match].arguments)})
  }.bind(this)

  /*
    Init
  */
  if(initialRoutes) this.addRoutes(initialRoutes)
  generateRegexTests()

}

if (module)
  if(module.exports)
    module.exports = Router
